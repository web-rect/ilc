


const $modalMenuJs = document.querySelector('.modal-menu-js'),
$menuMobileJs = document.querySelector('.menu-mobile-js'),
$modalContact = document.querySelector('.modal-contact'),
$btnJs = document.querySelectorAll('.btn-js'),
$modalContactGg = document.querySelector('.modal-contact-bg'),
$closeJs = document.querySelector('.close-js'),
$langDropJs = document.querySelector('.lang-drop-js'),
$btnL = document.querySelectorAll('.btn-l'),
$sliderJsTabs = document.querySelectorAll('.slider-js-tabs'),
$sectionLawyer = document.querySelector('.section-lawyer'),
$formJs = document.querySelector('.form-js'),
$modalUvedomlenie = document.querySelector('.modal-uvedomlenie'),
$menuMobileClick = document.querySelector('.menu-mobile-click'),
$menuMobileClickA = document.querySelectorAll('.menu-mobile-click a');

$(function () {
  $("a[href^='#']").click(function () {
      var _href = $(this).attr("href");
      setTimeout(() => {
        $modalMenuJs.classList.remove('active');
      }, 200);
      $("html, body").animate({scrollTop: $(_href).offset().top -150 + "px"}, 1500);
      return false;
  });
});

if ($btnJs) {
  $btnJs.forEach(item => {
    item.addEventListener('click', (e) => {
        e.preventDefault();
        $modalContact.classList.add('active');
        $modalContactGg.addEventListener('click', () => {
          $modalContact.classList.remove('active');
        });
        $closeJs.addEventListener('click', () => {
          $modalContact.classList.remove('active');
        });
    });
});
}


if ($menuMobileJs) {
  $menuMobileJs.addEventListener('click', (e) => {
    e.preventDefault();
    $modalMenuJs.classList.toggle('active');
});
}


if ($langDropJs) {
  $langDropJs.addEventListener('click',  () => {
    $langDropJs.classList.toggle('active');
  });
  
}

$(document).ready(function () {
  $(".form-js").submit(function (e) {

      e.preventDefault();
      var form_data = $(this).serialize(); 
      $.ajax({
          type: "POST", 
          url: "send.php", 
          data: form_data,
          success: function () {
              $modalUvedomlenie.classList.add('active');
              setTimeout(() => {
                $modalUvedomlenie.classList.remove('active');
              }, 3000);
              $formJs.reset();
          },
      });
  });
});

// tabs-slider
if ($sectionLawyer) {
  $sectionLawyer.addEventListener('click', (e) => {
    e.preventDefault();
    if (e.target.closest('.btn-l')) {
      $btnL.forEach(item => {
        item.classList.remove('active');
      });
      $sliderJsTabs.forEach(elem => {
        elem.classList.remove('active');
        if (e.target.closest('.btn-l').dataset.sliderTab == elem.dataset.sliderTab) {
          elem.classList.add('active');
          e.target.closest('.btn-l').classList.add('active');
        }
      });
    }
  });
  
}

new Swiper('.swiper-container', {

    spaceBetween: 0,
    loop: true,
    pagination: {
      el: '.swiper-pagination',
      clickable: true,
    },
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
    breakpoints: {

        320: {
          slidesPerView: 1,
          spaceBetween: 0
        },
   
        480: {
          slidesPerView: 1,
          spaceBetween: 0
        },
 
        640: {
          slidesPerView: 3,
          spaceBetween: 0
        },
        990: {
          slidesPerView: 2.5,
          spaceBetween: 0
        },
        1200: {
          slidesPerView: 3,
          spaceBetween: 0
        },
        1450: {
          slidesPerView: 3,
          spaceBetween: 0
        },
        1920: {
          slidesPerView: 4,
          spaceBetween: 0
        }
      }
});

new Swiper('.swiper-container-urist', {

    spaceBetween: 0,
    loop: true,
    pagination: {
      el: '.swiper-pagination',
      clickable: true,
    },
    slideToClickedSlide: true,
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
    breakpoints: {

        320: {
          slidesPerView: 1.2,
          spaceBetween: 10,
        },
   
        480: {
          slidesPerView: 1.5,
          spaceBetween: 10,
        },
 
        640: {
          slidesPerView: 2.5,
          spaceBetween: 10,
        },
        990: {
          slidesPerView: 3.5,
          spaceBetween: 10,
        },
        1200: {
          slidesPerView: 4,
          spaceBetween: 10,
        },
        1450: {
          slidesPerView: 5,
          spaceBetween: 10,
        },
        1920: {
          slidesPerView: 6.3,
          spaceBetween: 10,
        }
      }
});
new Swiper('.swiper-mobile', {

    spaceBetween: 0,
    loop: true,

    breakpoints: {

        320: {
          slidesPerView: 1.1,
          spaceBetween: 30
        },
   
        480: {
          slidesPerView: 1.2,
          spaceBetween: 30
        },
 
        640: {
          slidesPerView: 2.2,
          spaceBetween: 30
        },
        990: {
          slidesPerView: 2.2,
          spaceBetween: 30
        }

      }
});
  
new Swiper('.swiper-mobile-main', {

    spaceBetween: 0,
    loop: true,

    breakpoints: {

        320: {
          slidesPerView: 1.2,
          spaceBetween: 30
        },
   
        480: {
          slidesPerView: 1.3,
          spaceBetween: 30
        },
 
        640: {
          slidesPerView: 2.5,
          spaceBetween: 30
        },
        990: {
          slidesPerView: 2.5,
          spaceBetween: 30
        }

      }
});
  
new Swiper('.swiper-mobile-tarif', {

    spaceBetween: 0,
    loop: true,
    centeredSlides: true,
    breakpoints: {

        320: {
          slidesPerView: 2.2,
          spaceBetween: 15
        },
   
        480: {
          slidesPerView: 2.2,
          spaceBetween: 15
        },
 
        640: {
          slidesPerView: 2.5,
          spaceBetween: 15
        },
        990: {
          slidesPerView: 2.5,
          spaceBetween: 15
        }

      }
});
  