<?php

if (isset($_POST['email'])) {$email = $_POST['email'];}
if (isset($_POST['phone'])) {$phone = $_POST['phone'];}

// отправка нескольким адресатам
$to  = 'appsfork@gmail.com' . ','; // кому отправляем
$to .= 'Info@ilegalcenter.com' . ', '; 

$subject = "Сообщение с сайта ILC";
$message = '
<table bgcolor="#272828" width="100%" style="min-width: 320px; background-color: rgb(39, 40, 40) !important;" cellspacing="0" cellpadding="0">
  <tbody style="border-color: rgb(255, 255, 255) !important;">
    <tr style="border-color: rgb(255, 255, 255) !important;">
      <td style="padding: 20px 0px; border-color: rgb(31, 31, 31) !important;">
        <table width="600" align="center" style="max-width: 600px; margin: 0px auto; width: 100% !important; border-color: rgb(255, 255, 255) !important;" cellpadding="0" cellspacing="0">
          <tbody style="border-color: rgb(255, 255, 255) !important;">
            <tr style="border-color: rgb(255, 255, 255) !important;">
              <td bgcolor="#1F1F1F" style="border-radius: 6px; background-color: rgb(31, 31, 31) !important; border-color: rgb(50, 50, 50) !important;">
                <table width="100%" cellpadding="0" cellspacing="0" style="border-color: rgb(255, 255, 255) !important;">

                  <tbody style="border-color: rgb(255, 255, 255) !important;">
                    <tr style="border-color: rgb(255, 255, 255) !important;">
                      <td style="padding: 12px 18px 11px; border-bottom-width: 1px; border-bottom-style: solid; border-color: rgb(31, 31, 31) !important;">
                        <table width="100%" cellpadding="0" cellspacing="0" style="border-color: rgb(255, 255, 255) !important;">
                          <tbody style="border-color: rgb(255, 255, 255) !important;">
                            <tr style="border-color: rgb(255, 255, 255) !important;">
  
                              <td align="center" style="border-color: rgb(31, 31, 31) !important; width: 100%;">

                                <img src="https://ilegalcenter.com/img/logo.png" width="52" style="vertical-align: top; font: bold 20px / 22px &quot;San Francisco&quot;, Segoe, Roboto, Arial, Helvetica, sans-serif; color: rgb(255, 255, 255) !important;" alt="ILC">

                              </td>

                              <td width="47" style="width: 47px; max-width: 47px; border-color: rgb(31, 31, 31) !important;"></td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>

                    <tr style="border-color: rgb(255, 255, 255) !important;">
                      <td style="padding: 34px 10px 0px; border-color: rgb(31, 31, 31) !important;">
                        <table width="554" align="center" style="max-width: 554px; margin: 0px auto; width: 100% !important; border-color: rgb(255, 255, 255) !important;" cellpadding="0" cellspacing="0">

                          <tbody style="border-color: rgb(255, 255, 255) !important;">
                            <tr style="border-color: rgb(255, 255, 255) !important;">
                              <td class="pt-34_mr_css_attr pb-17_mr_css_attr lh-34_mr_css_attr" style="padding: 16px 0px 30px; text-align:center !important; font: 300 30px / 42px &quot;San Francisco&quot;, Segoe, Roboto, Arial, Helvetica, sans-serif; color: rgb(209, 209, 209) !important;">
                                Сообщение с сайта ILC
                              </td>
                            </tr>

                            <tr style="border-color: rgb(255, 255, 255) !important;">
                              <td style="border-color: rgb(31, 31, 31) !important;">
                                <table width="100%" cellpadding="0" cellspacing="0" style="border-color: rgb(255, 255, 255) !important;">

                                  <tbody style="border-color: rgb(255, 255, 255) !important;">
                                    <tr style="border-color: rgb(255, 255, 255) !important;">
                                      <td class="fs-15_mr_css_attr" valign="top" width="50%" style="padding: 20px 0px 16px; font: 17px / 24px &quot;San Francisco&quot;, Segoe, Roboto, Arial, Helvetica, sans-serif; color: rgb(209, 209, 209) !important; border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: rgb(31, 31, 31) !important;">
                                        Почта:
                                      </td>

                                      <td class="fs-15_mr_css_attr" width="30%" valign="top" align="right" style="padding: 20px 0px 16px; font: 17px / 24px &quot;San Francisco&quot;, Segoe, Roboto, Arial, Helvetica, sans-serif; color: rgb(209, 209, 209) !important; border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: rgb(31, 31, 31) !important;">
                                       ' . $email . ' 
                                      </td>

                                    </tr>

                                    <tr style="border-color: rgb(255, 255, 255) !important;">

                                      <td class="fs-15_mr_css_attr" valign="top" width="50%" style="padding: 16px 0px 20px; font: 17px / 24px &quot;San Francisco&quot;, Segoe, Roboto, Arial, Helvetica, sans-serif; color: rgb(209, 209, 209) !important; border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: rgb(31, 31, 31) !important;">
                                        Телефон:
                                      </td>

                                      <td class="fs-15_mr_css_attr" width="40%" valign="top" align="right" style="padding: 16px 0px 20px; font: 17px / 24px &quot;San Francisco&quot;, Segoe, Roboto, Arial, Helvetica, sans-serif; color: rgb(209, 209, 209) !important; border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: rgb(31, 31, 31) !important;">
                                       ' . $phone . '
                                      </td>
                                    </tr>

                                  </tbody>
                                </table>
                                <style>
                                  @media only screen and (max-width: 500px) {
                                    .cl_853031 .fs-15_mr_css_attr {
                                      font-size: 15px !important;
                                    }
                                    .cl_853031 .pb-29_mr_css_attr {
                                      padding-bottom: 29px !important;
                                    }
                                    .cl_853031 .pb-47_mr_css_attr {
                                      padding-bottom: 47px !important;
                                    }
                                  }
                                </style>

                              </td>
                            </tr>

                          </tbody>
                        </table>
                        <style>
                          @media only screen and (max-width: 500px) {
                            .cl_853031 .flexible_mr_css_attr {
                              width: 100% !important;
                            }
                            .cl_853031 .active-cta_mr_css_attr {}
                            .cl_853031 .fs-15_mr_css_attr {
                              font-size: 15px !important;
                            }
                            .cl_853031 .lh-21_mr_css_attr {
                              line-height: 21px !important;
                            }
                            .cl_853031 .lh-34_mr_css_attr {
                              line-height: 34px !important;
                            }
                            .cl_853031 .pt-17_mr_css_attr {
                              padding-top: 17px !important;
                            }
                            .cl_853031 .pt-34_mr_css_attr {
                              padding-top: 34px !important;
                            }
                            .cl_853031 .pb-17_mr_css_attr {
                              padding-bottom: 17px !important;
                            }
                          }
                        </style>
                      </td>

                    </tr>

                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
      </td>
    </tr>
  </tbody>
</table>

';

// устанавливаем тип сообщения Content-type, если хотим
$headers  = 'MIME-Version: 1.0' . "\r\n";
$headers .= "Content-type: text/html; charset=utf-8 \r\n";

// дополнительные данные

mail($to, $subject, $message, $headers);
?>